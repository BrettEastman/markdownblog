export async function load({ params }) {
	// params.slug is called that because the file is called [slug].svelte. If it were called [foo].svelte, it would be params.foo
	// TO DO: wrap this in a try/catch block and return a 404 page if it fails
	const post = await import(`../${params.slug}.md`);
	const { title, date } = post.metadata;
	const content = post.default;

	return {
		content,
		title,
		date
	};
}
